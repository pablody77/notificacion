"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *  Servicios de escucha de eventos rabbit
 */
const env = require("../server/environment");
const token = require("../token");
const fanoutConsumer_1 = require("./tools/fanoutConsumer");
const conf = env.getConfig(process.env);
function init() {
    const fanout = new fanoutConsumer_1.RabbitFanoutConsumer("auth");
    fanout.addProcessor("logout", processLogout);
    fanout.init();
}
exports.init = init;
/**
 * @api {fanout} auth/logout Logout de Usuarios
 * @apiGroup RabbitMQ GET
 *
 * @apiDescription Escucha de mensajes logout desde auth.
 *
 * @apiSuccessExample {json} Mensaje
 *     {
 *        "type": "logout",
 *        "message": "{tokenId}"
 *     }
 */
function processLogout(rabbitMessage) {
    console.log("RabbitMQ Consume logout " + rabbitMessage.message);
    token.invalidate(rabbitMessage.message);
}
//# sourceMappingURL=logoutService.js.map