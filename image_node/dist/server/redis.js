"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redis = require("ioredis");
const appConfig = require("./environment");
const conf = appConfig.getConfig(process.env);
let redisClient;
function getRedisDocument(id) {
    return new Promise((resolve, reject) => {
        getClient().get(escape(id), function (err, reply) {
            if (err)
                reject(err);
            if (!reply)
                reject(undefined);
            resolve(reply);
        });
    });
}
exports.getRedisDocument = getRedisDocument;
function setRedisDocument(id, image) {
    return new Promise((resolve, reject) => {
        getClient().set(id, image, function (err, reply) {
            if (err)
                reject(err);
            resolve(id);
        });
    });
}
exports.setRedisDocument = setRedisDocument;
function getClient() {
    if (!redisClient) {
        redisClient = new redis(conf.redisPort, conf.redisHost);
        redisClient.on("connect", function () {
            console.log("Redis conectado");
        });
        redisClient.on("end", function () {
            redisClient = undefined;
            console.error("Redis desconectado");
        });
    }
    return redisClient;
}
//# sourceMappingURL=redis.js.map