"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid = require("uuid/v1");
const redis = require("../server/redis");
function create(body) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            validateCreate(body)
                .then(body => {
                const image = {
                    id: uuid(),
                    image: body.image
                };
                redis.setRedisDocument(image.id, image.image)
                    .then(id => resolve(id))
                    .catch(err => reject(err));
            })
                .catch(err => reject(err));
        });
    });
}
exports.create = create;
function validateCreate(body) {
    const result = {
        messages: []
    };
    if (!body.image) {
        result.messages.push({ path: "image", message: "No puede quedar vacío." });
    }
    if (body.image.indexOf("data:image/") < 0) {
        result.messages.push({ path: "image", message: "Imagen invalida" });
    }
    if (result.messages.length > 0) {
        return Promise.reject(result);
    }
    return Promise.resolve(body);
}
//# sourceMappingURL=create.js.map