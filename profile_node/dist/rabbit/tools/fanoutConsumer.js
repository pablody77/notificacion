"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 *  Servicios de escucha de eventos rabbit
 */
const amqp = require("amqplib");
const env = require("../../server/environment");
class RabbitFanoutConsumer {
    constructor(exchange) {
        this.exchange = exchange;
        this.conf = env.getConfig(process.env);
        this.processors = new Map();
    }
    addProcessor(type, processor) {
        this.processors.set(type, processor);
    }
    /**
     * Escucha eventos específicos de cart.
     *
     * article-exist : Es un evento que lo envía Catalog indicando que un articulo existe y es valido para el cart.
     */
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const conn = yield amqp.connect(this.conf.rabbitUrl);
                const channel = yield conn.createChannel();
                channel.on("close", function () {
                    console.error("RabbitMQ  " + this.exchange + "  conexión cerrada, intentado reconecta en 10'");
                    setTimeout(() => this.init(), 10000);
                });
                console.log("RabbitMQ  " + this.exchange + "  conectado");
                const exchange = yield channel.assertExchange(this.exchange, "fanout", { durable: false });
                const queue = yield channel.assertQueue("", { exclusive: true });
                channel.bindQueue(queue.queue, exchange.exchange, "");
                channel.consume(queue.queue, (message) => {
                    const rabbitMessage = JSON.parse(message.content.toString());
                    if (this.processors.has(rabbitMessage.type)) {
                        this.processors.get(rabbitMessage.type)(rabbitMessage);
                    }
                }, { noAck: true });
            }
            catch (err) {
                console.error("RabbitMQ " + this.exchange + " : " + err.message);
                setTimeout(() => this.init(), 10000);
            }
        });
    }
}
exports.RabbitFanoutConsumer = RabbitFanoutConsumer;
//# sourceMappingURL=fanoutConsumer.js.map