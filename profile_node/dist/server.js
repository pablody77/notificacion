"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logoutService = require("./rabbit/logoutService");
const env = require("./server/environment");
const express = require("./server/express");
// Variables de entorno
const conf = env.getConfig(process.env);
// Mejoramos el log de las promesas
process.on("unhandledRejection", (reason, p) => {
    console.error("Unhandled Rejection at: Promise", p, "reason:", reason);
});
// Se configura e inicia express
const app = express.init(conf);
logoutService.init();
app.listen(conf.port, () => {
    console.log(`Image Server escuchando en puerto ${conf.port}`);
});
module.exports = app;
//# sourceMappingURL=server.js.map