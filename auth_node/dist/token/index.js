"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("./passport");
var service_1 = require("./service");
exports.create = service_1.create;
exports.invalidate = service_1.invalidate;
exports.initPassport = passport.init;
//# sourceMappingURL=index.js.map