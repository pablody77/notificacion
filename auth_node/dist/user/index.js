"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var service_1 = require("./service");
exports.register = service_1.register;
exports.login = service_1.login;
exports.findById = service_1.findById;
exports.changePassword = service_1.changePassword;
exports.hasPermission = service_1.hasPermission;
exports.grant = service_1.grant;
exports.revoke = service_1.revoke;
exports.enable = service_1.enable;
exports.disable = service_1.disable;
exports.findAll = service_1.findAll;
//# sourceMappingURL=index.js.map