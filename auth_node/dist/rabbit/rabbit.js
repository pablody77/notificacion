"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const amqp = require("amqplib");
const env = require("../server/environment");
const conf = env.getConfig(process.env);
let channel;
/**
 * @api {fanout} auth/fanout Invalidar Token
 * @apiGroup RabbitMQ POST
 *
 * @apiDescription AuthService enviá un broadcast a todos los usuarios cuando un token ha sido invalidado. Los clientes deben eliminar de sus caches las sesiones invalidadas.
 *
 * @apiSuccessExample {json} Mensaje
 *     {
 *        "type": "logout",
 *        "message": "{Token revocado}"
 *     }
 */
function sendLogout(token) {
    return sendMessage({
        type: "logout",
        message: token
    });
}
exports.sendLogout = sendLogout;
function sendMessage(message) {
    return __awaiter(this, void 0, void 0, function* () {
        const channel = yield getChannel();
        try {
            const exchange = yield channel.assertExchange("auth", "fanout", { durable: false });
            if (channel.publish(exchange.exchange, "", new Buffer(JSON.stringify(message)))) {
                return Promise.resolve(message);
            }
            else {
                return Promise.reject(new Error("No se pudo encolar el mensaje"));
            }
        }
        catch (err) {
            console.log("RabbitMQ " + err);
            return Promise.reject(err);
        }
    });
}
function getChannel() {
    return __awaiter(this, void 0, void 0, function* () {
        if (!channel) {
            try {
                const conn = yield amqp.connect(conf.rabbitUrl);
                channel = yield conn.createChannel();
                console.log("RabbitMQ conectado");
                channel.on("close", function () {
                    console.error("RabbitMQ Conexión cerrada");
                    channel = undefined;
                });
            }
            catch (onReject) {
                console.error("RabbitMQ " + onReject.message);
                channel = undefined;
                return Promise.reject(onReject);
            }
        }
        if (channel) {
            return Promise.resolve(channel);
        }
        else {
            return Promise.reject(new Error("No channel available"));
        }
    });
}
//# sourceMappingURL=rabbit.js.map