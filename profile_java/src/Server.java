
import application.RabbitController;
import application.RestController;
import spark.Spark;
import utils.db.MongoStore;
import utils.errors.ErrorHandler;
import utils.server.CorsFilter;
import utils.server.Environment;

public class Server {

    public static void main(String[] args) {
        new Server().start();
    }

    void start() {
        MongoStore.init();

        Spark.exception(Exception.class, (ex, req, res) -> ErrorHandler.handleInternal(ex, req, res)); //son execptiones de error 
        Spark.port(Environment.getEnv().serverPort);  //obtiene el puerto en el que se va a trabajar
        Spark.staticFiles.location(Environment.getEnv().staticLocation);
        CorsFilter.apply();

        Spark.post("/v1/profiles", (req, res) -> RestController.addProfile(req, res));//Agregar un profile
        Spark.get("/v1/profiles/", (req, res) -> RestController.getProfile(req, res));//Obtiene un Perfil del usuario logueado
        Spark.get("/v1/profiles/:profileId", (req, res) -> RestController.getProfilebyID(req, res));//Obtiene un Perfil del usuario logueado

        RabbitController.init();
    }

}