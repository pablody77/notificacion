define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./www/main.js",
    "group": "C__Users_viznn_Documentos_ecommerce_master_notificacion_java_www_main_js",
    "groupTitle": "C__Users_viznn_Documentos_ecommerce_master_notificacion_java_www_main_js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/v1/notifications/:notificationId",
    "title": "Buscar Notificacion",
    "name": "Buscar_Orden",
    "group": "Ordenes",
    "description": "<p>Busca una order del usuario logueado, por su id.</p>",
    "success": {
      "examples": [
        {
          "title": "Respuesta",
          "content": "HTTP/1.1 200 OK\n{\n   \"id\": \"{orderID}\",\n   \"status\": \"{Status}\",\n   \"cartId\": \"{cartId}\",\n   \"updated\": \"{updated date}\",\n   \"created\": \"{created date}\",\n   \"articles\": [\n      {\n          \"id\": \"{articleId}\",\n          \"quantity\": {quantity},\n          \"validated\": true|false,\n          \"valid\": true|false\n      }, ...\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./src/application/RestController.java",
    "groupTitle": "Ordenes",
    "examples": [
      {
        "title": "Header Autorización",
        "content": "Authorization=bearer {token}",
        "type": "String"
      }
    ],
    "error": {
      "examples": [
        {
          "title": "401 Unauthorized",
          "content": "HTTP/1.1 401 Unauthorized",
          "type": "json"
        },
        {
          "title": "400 Bad Request",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"path\" : \"{Nombre de la propiedad}\",\n    \"message\" : \"{Motivo del error}\"\n}",
          "type": "json"
        },
        {
          "title": "400 Bad Request",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"error\" : \"{Motivo del error}\"\n}",
          "type": "json"
        },
        {
          "title": "500 Server Error",
          "content": "HTTP/1.1 500 Server Error\n{\n    \"error\" : \"{Motivo del error}\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "direct",
    "url": "order/add-notificacion",
    "title": "Crear Orden",
    "group": "RabbitMQ",
    "examples": [
      {
        "title": "Mensaje",
        "content": "{\n\"type\": \"place-order\",\n\"exchange\" : \"{Exchange name to reply}\"\n\"queue\" : \"{Queue name to reply}\"\n\"message\" : {\n    \"cartId\": \"{cartId}\",\n    \"articles\": \"[articleId, ...]\",\n}",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "./src/application/RabbitController.java",
    "groupTitle": "RabbitMQ",
    "name": "DirectOrderAddNotificacion"
  },
  {
    "type": "fanout",
    "url": "auth/logout",
    "title": "Logout",
    "group": "RabbitMQ_GET",
    "description": "<p>Escucha de mensajes logout desde auth. Invalida sesiones en cache.</p>",
    "examples": [
      {
        "title": "Mensaje",
        "content": "{\n  \"type\": \"logout\",\n  \"message\" : \"tokenId\"\n}",
        "type": "json"
      }
    ],
    "version": "0.0.0",
    "filename": "./src/application/RabbitController.java",
    "groupTitle": "RabbitMQ_GET",
    "name": "FanoutAuthLogout"
  }
] });
