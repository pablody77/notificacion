<a name="top"></a>
# Order Service en Java v0.1.0

Microservicio de Ordenes

- [Ordenes](#ordenes)
	- [Buscar Notificacion](#buscar-notificacion)
	
- [RabbitMQ](#rabbitmq)
	- [Crear Orden](#crear-orden)
	
- [RabbitMQ_GET](#rabbitmq_get)
	- [Logout](#logout)
	


# <a name='ordenes'></a> Ordenes

## <a name='buscar-notificacion'></a> Buscar Notificacion
[Back to top](#top)

<p>Busca una order del usuario logueado, por su id.</p>

	GET /v1/notifications/:notificationId



### Examples

Header Autorización

```
Authorization=bearer {token}
```


### Success Response

Respuesta

```
HTTP/1.1 200 OK
{
   "id": "{orderID}",
   "status": "{Status}",
   "cartId": "{cartId}",
   "updated": "{updated date}",
   "created": "{created date}",
   "articles": [
      {
          "id": "{articleId}",
          "quantity": {quantity},
          "validated": true|false,
          "valid": true|false
      }, ...
  ]
}
```


### Error Response

401 Unauthorized

```
HTTP/1.1 401 Unauthorized
```
400 Bad Request

```
HTTP/1.1 400 Bad Request
{
    "path" : "{Nombre de la propiedad}",
    "message" : "{Motivo del error}"
}
```
400 Bad Request

```
HTTP/1.1 400 Bad Request
{
    "error" : "{Motivo del error}"
}
```
500 Server Error

```
HTTP/1.1 500 Server Error
{
    "error" : "{Motivo del error}"
}
```
# <a name='rabbitmq'></a> RabbitMQ

## <a name='crear-orden'></a> Crear Orden
[Back to top](#top)



	DIRECT order/add-notificacion



### Examples

Mensaje

```
{
"type": "place-order",
"exchange" : "{Exchange name to reply}"
"queue" : "{Queue name to reply}"
"message" : {
    "cartId": "{cartId}",
    "articles": "[articleId, ...]",
}
```




# <a name='rabbitmq_get'></a> RabbitMQ_GET

## <a name='logout'></a> Logout
[Back to top](#top)

<p>Escucha de mensajes logout desde auth. Invalida sesiones en cache.</p>

	FANOUT auth/logout



### Examples

Mensaje

```
{
  "type": "logout",
  "message" : "tokenId"
}
```




