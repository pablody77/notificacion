package notificacion.vo;

import com.google.gson.annotations.SerializedName;

import utils.gson.Builder;
import utils.gson.JsonSerializable;
import utils.validator.MaxLen;
import utils.validator.MinLen;
import utils.validator.Required;

/**
 * Objeto valor para artículos.
 */
public class ArticleData implements JsonSerializable {
    @SerializedName("_id")
    public String id;

    @SerializedName("name")
    @Required()
    @MinLen(1)
    @MaxLen(60)
    public String name;

    @SerializedName("description")
    @MaxLen(2048)
    public String description;

     @SerializedName("asunto")
     @MaxLen(60)
    public String asunto;

    @SerializedName("correodestino")
    @MaxLen(60)
    public String correodestino;


    @Override
    public String toJson() {
        return Builder.gson().toJson(this);
    }
}