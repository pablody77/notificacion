package notificacion;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.query.Query;

import spark.utils.StringUtils;
import utils.db.MongoStore;
import utils.errors.ValidationError;

public class NotificacionRepository {
    private static NotificacionRepository instance;

    private NotificacionRepository() {

    }

    public static NotificacionRepository getInstance() {
        if (instance == null) {
            instance = new NotificacionRepository();
        }
        return instance;
    }

    public Notificacion save(Notificacion noti) {
        MongoStore.getDataStore().save(noti);
        return noti;
    }

    public Notificacion get(String id) throws ValidationError {
        if (StringUtils.isBlank(id)) {
            throw new ValidationError().addPath("id", "Not found");
        }

        Notificacion result = MongoStore.getDataStore().get(Notificacion.class, new ObjectId(id));
        if (result == null) {
            throw new ValidationError().addPath("id", "Not found");
        }

        return result;
    }

    public List<Notificacion> findByCriteria(String criteria) {
        ArrayList<Notificacion> result = new ArrayList<>();
        Query<Notificacion> q = MongoStore.getDataStore().createQuery(Notificacion.class);
        q.and(q.criteria("enabled").equal(true), //
                q.or(q.criteria("description.name").contains(criteria),
                        q.criteria("description.description").contains(criteria)));

        Iterable<Notificacion> resultList = q.fetch();
        for (Notificacion article : resultList) {
            result.add(article);
        }
        return result;
    }
}