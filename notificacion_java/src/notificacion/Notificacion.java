package notificacion;

//import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import notificacion.vo.ArticleData;
//import notificacion.vo.NewData;
import utils.errors.ValidationError;
import utils.validator.Validator;

/**
 * Es el Agregado principal de Articulo.
 */
@Entity("notificacion")
public class Notificacion {
    @Id
    private ObjectId id;

    private Description description;


    private Notificacion() {

    }

    /**
     * Crea una nueva notificacion
     */
    public static Notificacion newArticle(notificacion.vo.NewData data) throws ValidationError {
        Validator.validate(data);

        Notificacion noti = new Notificacion();

        noti.description = new Description();
        noti.description.name = data.name;
        noti.description.description = data.description;
        noti.description.correodestino = data.correodestino;
        noti.description.asunto = data.asunto;

        return noti;
    }

    /**
     * Actualiza la descripción de un articulo.
     */
    /*public void updateDescription(DescriptionData data) throws ValidationError {
        Validator.validate(data);

        this.description.name = data.name;
        this.description.description = data.description;
        this.description.image = data.image;
        this.updated = new java.util.Date();
    }*/

    /**
     * Actualiza el precio de un articulo.
     
    public void updatePrice(double price) throws ValidationError {
        if (price < 0) {
            throw new ValidationError().addPath("price", "Inválido");
        }

        this.price = price;
        this.updated = new java.util.Date();
    }*/

    /**
     * Actualiza el stock actual de un articulo.
     
    public void updateStock(int stock) throws ValidationError {
        if (stock < 0) {
            throw new ValidationError().addPath("stock", "Inválido");
        }

        this.stock = stock;
        this.updated = new java.util.Date();
    }*/

    /**
     * Deshabilita el articulo para que no se pueda usar mas
    
    public void disable() {
        this.enabled = false;
        this.updated = new java.util.Date();
    }

    public boolean enabled() {
        return enabled;
    } */

    /**
     * Obtiene una representación interna de los valores.
     * Preserva la inmutabilidad de la entidad.
    */
    public ArticleData value() {
        ArticleData data = new ArticleData();
        data.id = this.id.toHexString();

        data.name = this.description.name;
        data.description = this.description.description;
        data.asunto = this.description.asunto;
        data.correodestino = this.description.correodestino;
        
        return data;
    } 

    static class Description {
        String name;
        String description;
        String correodestino;
        String asunto;

    }
}
