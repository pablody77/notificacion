package events;

import javax.xml.bind.ValidationException;

import events.schema.Event;
import events.schema.NewPlaceData;
import events.schema.PlaceEvent;
import projections.ProjectionService;
import utils.errors.ValidationError;
import utils.validator.Validator;

public class EventService {
    static EventService instance;

    public static EventService getInstance() {
        if (instance == null) {
            instance = new EventService();
        }
        return instance;
    }

    public Event placeOrder(NewPlaceData data) throws ValidationException, ValidationError {
        Validator.validate(data);

        EventRepository repository = EventRepository.getInstance();
        Event event = null;
        if (event == null) {
            PlaceEvent place = new PlaceEvent(data.asunto, data.correodestino, data.descripcion);

            event = Event.newPlaceOrder(place);
            repository.save(event);
            //ProjectionService.getInstance().(event);
        }

        return event;
    }
}