package events.schema;

import com.google.gson.annotations.SerializedName;

import utils.gson.Builder;
import utils.validator.MinLen;
import utils.validator.Required;

public class NewPlaceData {
    @SerializedName("correodestino")
    @Required
    @MinLen(1)
    public String correodestino;

    @SerializedName("asunto")
    @Required
    @MinLen(1)
    public String asunto;

    @SerializedName("descripcion")
    @Required
    @MinLen(1)
    public String descripcion;

    public static NewPlaceData fromJson(String json) {
        return Builder.gson().fromJson(json, NewPlaceData.class);
    }
}
