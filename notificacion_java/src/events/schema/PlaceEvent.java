package events.schema;

public class PlaceEvent {
    private String correodestino;
    private String asunto;
    private String descripcion;

    public PlaceEvent() {
    }

    public PlaceEvent(String correodestino, String asunto, String descripcion) {
        this.correodestino = correodestino;
        this.asunto = asunto;
        this.descripcion = descripcion;
    }

    public String getCorreodestino() {
        return correodestino;
    }

    public String getAsunto() {
        return asunto;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
