package events.schema;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

/**
 * Permite almacenar los eventos del event store.
 */
@Entity(value = "event", noClassnameStored = true)
public class Event {
    @Id
    private ObjectId id;

    @Indexed()
    private ObjectId notificacionId;

    private EventType type;

    private PlaceEvent placeEvent;


    private Event() {
    }

    // Crea un nuevo evento de place order
    public static Event newPlaceOrder(PlaceEvent placeEvent) {
        Event result = new Event();
        result.notificacionId = new ObjectId();
        result.type = EventType.PLACE_ORDER;
        result.placeEvent = placeEvent;
        return result;
    }


    public ObjectId getId() {
        return id;
    }

    public ObjectId getNotificacionId() {
        return notificacionId;
    }

    public EventType getType() {
        return type;
    }

    public PlaceEvent getPlaceEvent() {
        return placeEvent;
    }

}
