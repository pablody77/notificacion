package projections.order.schema;



import events.schema.Event;


public interface OrderUpdater {
    void update(Order order, Event event);

      

    public static class VoidEventUpdater implements OrderUpdater {
        @Override
        public void update(Order order, Event event) {

        }
    }

}
