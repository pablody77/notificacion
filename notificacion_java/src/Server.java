
import java.util.logging.Level;
import java.util.logging.Logger;

import application.RabbitController;
import application.RestController;
import spark.Spark;
import utils.errors.ErrorHandler;
import utils.errors.SimpleError;
import utils.server.CorsFilter;
import utils.server.Environment;

public class Server {

    public static void main(String[] args) {
        new Server().start();
    }

    void start() {
        Spark.notFound((req, res) -> ErrorHandler.handleInternal(new SimpleError(404, "Not Found"), req, res));
        Spark.exception(Exception.class, (ex, req, res) -> ErrorHandler.handleInternal(ex, req, res));
        Spark.port(Environment.getEnv().serverPort);// obtiene el puerto en el que va trabajar
        Spark.staticFiles.location(Environment.getEnv().staticLocation);
        CorsFilter.apply();

        RabbitController.init();
        
        Spark.get("/v1/notifications/", (req, res) -> RestController.getNotifications(req, res));
        Spark.get("/v1/notifications/:notificationId", (req, res) -> RestController.getNotificationById(req, res));// busca una notificacion 
        

        Logger.getLogger("Validator").log(Level.INFO,
                "Order Service escuchando en el puerto : " + Environment.getEnv().serverPort);
    }
}
