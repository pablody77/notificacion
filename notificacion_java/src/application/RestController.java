package application;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.annotations.SerializedName;

import org.bson.types.ObjectId;
import org.mongodb.morphia.query.ValidationException;

import events.EventService;
import events.schema.PaymentData;
import events.schema.PaymentEvent;
import projections.common.Status;
import projections.order.OrderService;
import projections.order.schema.Order;
import projections.orderStatus.OrderStatusRepository;
import projections.orderStatus.schema.OrderStatus;
import security.TokenService;
import security.User;
import spark.Request;
import spark.Response;
import utils.errors.ErrorHandler;
import utils.errors.SimpleError;
import utils.errors.ValidationError;
import utils.gson.Builder;
import utils.gson.JsonSerializable;
import notificacion.Notificacion;
import notificacion.NotificacionRepository;

public class RestController {
    /**
     * @api {get} /v1/notifications/:notificationId Buscar Notificacion
     * @apiName Buscar Orden
     * @apiGroup Ordenes
     *
     * @apiDescription Busca una order del usuario logueado, por su id.
     *
     * @apiUse AuthHeader
     *
     * @apiSuccessExample {json} Respuesta
     *   HTTP/1.1 200 OK
     *   {
     *      "id": "{orderID}",
     *      "status": "{Status}",
     *      "cartId": "{cartId}",
     *      "updated": "{updated date}",
     *      "created": "{created date}",
     *      "articles": [
     *         {
     *             "id": "{articleId}",
     *             "quantity": {quantity},
     *             "validated": true|false,
     *             "valid": true|false
     *         }, ...
     *     ]
     *   }
     *
     * @apiUse Errors
     */
    public static String getNotificationById(Request req, Response res) {
        //return req.params(":notificationId");

        // try {
        //     User usr = TokenService.getUser(req.headers("Authorization"));

        //     // Order order = OrderService.getInstance().buildOrder(new ObjectId(req.params(":orderId")));
        //     // if (order == null) 
        //     //     throw new ValidationError().addPath("orderId", "Not Found");
        //     // }
        //     // if (!order.getUserId().equals(usr.id)) {
        //     //     return null;
        //     // }

        //     return "";
        // } catch (SimpleError | ValidationError e) {
        //     return ErrorHandler.handleError(res, e);
        // }

                try{

                    System.out.println(req.params(":notificationId"));
                Notificacion noti= NotificacionRepository.getInstance().get(req.params(":notificationId"));
    
                return noti.value().toJson();
            } catch (ValidationError e) {
                return ErrorHandler.handleError(res, e);
            }
        }
    

    public static String getNotifications(Request req, Response res) {
        //Aca nos traeria las notiifcaciones con un criterio que seria por algun correo en especifico.
    
        return "[{}]";
    }
    
    /*
    public static class OrderData implements JsonSerializable {
        @SerializedName("id")
        public String id;

        @SerializedName("status")
        public Status status;

        @SerializedName("cartId")
        public String cartId;

        @SerializedName("totalPrice")
        public double totalPrice;

        @SerializedName("totalPayment")
        public double totalPayment;

        public Date updated = new Date();
        public Date created = new Date();

        @SerializedName("articles")
        public Article[] articles = new Article[] {};

        @SerializedName("payment")
        public Payment[] payment = new Payment[] {};

        public OrderData(Order order) {
            id = order.getId().toHexString();
            status = order.getStatus();
            cartId = order.getCartId();
            totalPrice = order.getTotalPrice();
            totalPayment = order.getTotalPayment();
            updated = order.getUpdated();
            created = order.getCreated();
            articles = Arrays.stream(order.getArticles()).map(
                    a -> new Article(a.getId(), a.getQuantity(), a.isValidated(), a.isValid(), a.getUnitaryPrice()))
                    .collect(Collectors.toList()).toArray(new Article[] {});
            payment = Arrays.stream(order.getPayment()).map(a -> new Payment(a.getMethod(), a.getAmount()))
                    .collect(Collectors.toList()).toArray(new Payment[] {});

        }

        public static class Article {
            @SerializedName("id")
            public String id;

            @SerializedName("quantity")
            public int quantity;

            @SerializedName("unitaryPrice")
            public double unitaryPrice;

            @SerializedName("validated")
            public boolean validated;

            @SerializedName("valid")
            public boolean valid;

            public Article(String id, int quantity, boolean validated, boolean valid, double unitaryPrice) {
                this.id = id;
                this.quantity = quantity;
                this.validated = validated;
                this.valid = valid;
                this.unitaryPrice = unitaryPrice;
            }
        }

        public static class Payment {
            @SerializedName("method")
            public PaymentEvent.Method method;

            @SerializedName("amount")
            public double amount;

            public Payment(PaymentEvent.Method method, double amount) {
                this.method = method;
                this.amount = amount;
            }
        }

        @Override
        public String toJson() {
            return Builder.gson().toJson(this);
        }
    }

    public static class OrderListData implements JsonSerializable {
        @SerializedName("id")
        public String id;

        @SerializedName("status")
        public Status status;

        @SerializedName("cartId")
        public String cartId;

        @SerializedName("totalPrice")
        public double totalPrice;

        @SerializedName("totalPayment")
        public double totalPayment;

        public Date updated = new Date();
        public Date created = new Date();

        @SerializedName("articles")
        public int articles;

        public OrderListData(OrderStatus order) {
            id = order.getId().toHexString();
            status = order.getStatus();
            cartId = order.getCartId();
            totalPrice = order.getTotalPrice();
            totalPayment = order.getPayment();
            updated = order.getUpdated();
            created = order.getCreated();
            articles = order.getArticles();
        }

        @Override
        public String toJson() {
            return Builder.gson().toJson(this);
        }
    }*/

}
