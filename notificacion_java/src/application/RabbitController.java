package application;

import java.util.Arrays;
import java.util.stream.Collectors;

import com.google.gson.annotations.SerializedName;

import events.EventService;
import events.schema.Event;
import events.schema.NewArticleValidationData;
import events.schema.NewPlaceData;
import events.schema.PlaceEvent;
import events.schema.PlaceEvent.Article;
import security.TokenService;
import utils.rabbit.DirectConsumer;
import utils.rabbit.DirectPublisher;
import utils.rabbit.FanoutConsumer;
import utils.rabbit.RabbitEvent;
import utils.rabbit.TopicPublisher;

public class RabbitController {

    public static void init() {
        FanoutConsumer fanoutConsumer = new FanoutConsumer("auth");
        fanoutConsumer.addProcessor("logout", e -> RabbitController.logout(e));
        fanoutConsumer.start();

        DirectConsumer directConsumer = new DirectConsumer("order", "notification");
        directConsumer.addProcessor("add-notification", e -> RabbitController.agregarNotificacion(e));
        directConsumer.start();
    }

    /**
     * @api {fanout} auth/logout Logout
     *
     * @apiGroup RabbitMQ GET
     *
     * @apiDescription Escucha de mensajes logout desde auth. Invalida sesiones en cache.
     *
     * @apiExample {json} Mensaje
     *   {
     *     "type": "logout",
     *     "message" : "tokenId"
     *   }
     */
    public static void logout(RabbitEvent event) {
        TokenService.invalidate(event.message.toString());
    }

    /**
    *
    * @api {direct} order/add-notificacion Crear Notificacion
    *
    * @apiGroup RabbitMQ 
    *
    * @apiExample {json} Mensaje
    *     {
    *     "type": "place-order",
    *     "exchange" : "{Exchange name to reply}"
    *     "queue" : "{Queue name to reply}"
    *     "message" : {
    *         "cartId": "{cartId}",
    *         "articles": "[articleId, ...]",
    *     }
    */
    public static void agregarNotificacion(RabbitEvent event) {
        NewPlaceData cart = NewPlaceData.fromJson(event.message.toString());
        try {
            // Guardar la notification

            Event data = EventService.getInstance().placeOrder(cart);
           // sendOrderPlaced(data);
            
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    public static void sendArticleValidation(String orderId, String articleId) {
        //ArticleValidationData data = new ArticleValidationData(orderId, articleId);

        RabbitEvent eventToSend = new RabbitEvent();
        eventToSend.type = "article-data";
        eventToSend.exchange = "order";
        eventToSend.queue = "order";
        eventToSend.message = "mensaje";

        DirectPublisher.publish("catalog", "catalog", eventToSend);
    }


}